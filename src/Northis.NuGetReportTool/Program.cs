﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NDesk.Options;
using Northis.NuGetReportTool.Data;
using Northis.NuGetReportTool.Properties;

namespace Northis.NuGetReportTool
{
	/// <summary>
	/// Предоставляет точку входа программы.
	/// </summary>
	internal class Program
	{
		#region Enums
		private enum ExitCode
		{
			[Display(ResourceType = typeof(Resources), Description = nameof(Resources.StrSuccess))]
			Success = 0,

			[Display(ResourceType = typeof(Resources), Description = nameof(Resources.StrError))]
			Error = 1,

			[Display(ResourceType = typeof(Resources), Description = nameof(Resources.Unconsolidated))]
			Unconsolidated = 2
		}
		#endregion

		#region Data
		#region Static
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		#endregion
		#endregion

		#region Private
		private static (List<DependenciesGroup> Data, bool Consolidated) AddExtendedData(IReadOnlyCollection<DependenciesGroup> referencesGroups)
		{
			if (referencesGroups.Count == 1)
			{
				return (referencesGroups.ToList(), true);
			}

			var total = new DependenciesGroup
			{
				Type = GroupType.Total,
				Name = "Total",
				Dependencies = referencesGroups.SelectMany(g => g.Dependencies)
											   .Distinct()
											   .OrderBy(r => r.Id)
											   .ToArray()
			};

			var multiversionReferences = total.Dependencies.GroupBy(r => r.Id)
											  .Where(g => g.Count() > 1)
											  .SelectMany(g => g)
											  .ToArray();
			var result = new List<DependenciesGroup>
			{
				total
			};

			var consolidated = true;
			if (multiversionReferences.Any())
			{
				result.Add(new DependenciesGroup
				{
					Type = GroupType.Consolidate,
					Name = "Consolidate",
					Dependencies = multiversionReferences
				});
				consolidated = false;
			}

			result.AddRange(referencesGroups);
			return (result, consolidated);
		}

		private static IEnumerable ExpandRepeater(string name, object currentObject, (string GeneratedDate, IReadOnlyCollection<DependenciesGroup> RefGroups) context)
		{
			switch (currentObject)
			{
				case null:
					switch (name)
					{
						case TemplateVariables.GroupsRepeater:
							return context.RefGroups;
					}

					break;
				case DependenciesGroup group:
					switch (name)
					{
						case TemplateVariables.ReferenceRepeater:
							return group.Dependencies;
					}

					break;
			}

			return null;
		}

		private static string FillPlaceholder(string name, object currentObject, (string GeneratedDate, List<DependenciesGroup> RefGroups) context)
		{
			switch (currentObject)
			{
				case null:
					switch (name)
					{
						case TemplateVariables.GeneratedDateTime:
							return context.GeneratedDate;
					}

					break;
				case DependenciesGroup group:
					switch (name)
					{
						case TemplateVariables.Index:
							return context.RefGroups.IndexOf(group)
										  .ToString();
						case TemplateVariables.Name:
							return group.Name;
						case TemplateVariables.IsConsolidation:
							// ToDo заменить на репитер, т.к. этот код не должен знать о синтаксисе шаблона
							return group.Type == GroupType.Consolidate ? "consolidate" : string.Empty;
					}

					break;
				case Dependency reference:
					switch (name)
					{
						case TemplateVariables.ReferenceId:
							return reference.Id;
						case TemplateVariables.ReferenceVersion:
							return reference.Version;
					}

					break;
			}

			return null;
		}

		/// <summary>
		/// Выполняет код программы.
		/// </summary>
		/// <param name="args">Аргументы командной строки.</param>
		private static int Main(string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
				{
					var exc = (Exception)eventArgs.ExceptionObject;
					Logger.Fatal(exc, Resources.StrError_UnhandledException, exc.Message);
				};
			List<string> pathsForScan;
			string reportFilePath = null;
			var help = false;
			var optionSet = new OptionSet
			{
				{
					"r|report|report-path=", Resources.StrReportArgDescription, v => reportFilePath = v
				},
				{
					"h|help", Resources.StrHelpArgDescription, v => help = v != null
				}
			};
			try
			{
				pathsForScan = optionSet.Parse(args);
			}
			catch(OptionException e)
			{
				Logger.Error(e);
				Console.WriteLine(Resources.StrShowHelpAdvice);
				return (int)ExitCode.Error;
			}

			if (help)
			{
				Console.WriteLine(Resources.StrExampleOfUsage);
				Console.WriteLine(Resources.StrListOfAvailableArgs);
				optionSet.WriteOptionDescriptions(Console.Out);
				Console.WriteLine(Resources.StrListOfAvailableExitCodes);
				var exitCodeType = typeof(ExitCode);
				var exitCodes = Enum.GetValues(exitCodeType);
				foreach (var exitCode in exitCodes)
				{
					var field = exitCodeType.GetField(exitCode.ToString());

					var displayAttribute = field.GetCustomAttribute<DisplayAttribute>(false);

					Console.WriteLine(Resources.StrExitCodeStringFormat, (int)exitCode, exitCode, displayAttribute.GetDescription());
				}
				return (int)ExitCode.Success;
			}

			const string projectFileExtenstion = "*.csproj";
			if (pathsForScan.Count == 0)
			{
				throw new ArgumentException(string.Format(Resources.StrMustSetProjectPath, projectFileExtenstion, Resources.StrShowHelpAdvice));
			}

			Logger.Info(Resources.StrRetrievingReferencesDataStarted);
			var projectPaths = pathsForScan.SelectMany(path => Directory.GetFiles(path, projectFileExtenstion, SearchOption.AllDirectories));

			var projects = new List<DependenciesGroup>();
			foreach (var projectPath in projectPaths)
			{
				const string assetFileName = "project.assets.json";
				var objFolderPath = Path.Combine(projectPath, @"..\obj");
				var singleOrDefault = Directory.GetFiles(objFolderPath, assetFileName)
											   .SingleOrDefault();

				if (singleOrDefault == null) continue;

				using (var r = new StreamReader(singleOrDefault))
				{
					var json = r.ReadToEnd();
					var nugetReferences = AssetsFileParser.GetNugetReferences(json);
					var projectName = Path.GetFileNameWithoutExtension(projectPath);
					projects.Add(new DependenciesGroup
					{
						Type = GroupType.Project,
						Name = projectName,
						Dependencies = nugetReferences.OrderBy(x => x.Id)
													  .ToList()
					});
				}
			}

			Logger.Info(Resources.StrRetrievingReferencesDataComplete);

			projects = projects.OrderBy(x => x.Name)
							   .ToList();

			var (report, consolidated) = MakeReport(projects);

			if (report == null)
			{
				return (int)ExitCode.Success;
			}

			if (string.IsNullOrWhiteSpace(reportFilePath))
			{
				Logger.Warn(Resources.StrReportFileNameNotProvided_ReportWasNotSaved);
			}
			else
			{
				File.WriteAllText(reportFilePath, report, Encoding.UTF8);
				Logger.Info(Resources.StrReportWasSuccessfullySavedIntoFile, reportFilePath);
			}

			return (int)(consolidated ? ExitCode.Success : ExitCode.Unconsolidated);
		}

		private static (string Report, bool Consolidated) MakeReport(IReadOnlyCollection<DependenciesGroup> projects)
		{
			if (!projects.Any())
			{
				Logger.Warn(Resources.StrProjectsDoesNotUseProjectReference_ReportWasNotGenerated);
				return (null, true);
			}

			var builder = new ReportBuilder(AppSettings.PlaceholderBoundary, AppSettings.OpenRepeaterLeftBoundary, AppSettings.OpenRepeaterRightBoundary,
											AppSettings.CloseRepeaterLeftBoundary, AppSettings.CloseRepeaterRightBoundary);
			Logger.Info(Resources.StrReportGenerationStarted);
			var stopwatch = Stopwatch.StartNew();
			var template = File.ReadAllText(TemplateNames.Report);

			var (extendedData, consolidated) = AddExtendedData(projects);
			var context = (GeneratedDate: DateTime.Now.ToString(), RefGroups: extendedData);
			var report = builder.Build(template, (n, o) => FillPlaceholder(n, o, context), (n, o) => ExpandRepeater(n, o, context));
			stopwatch.Stop();
			Logger.Info(Resources.StrReportGenerationComplete_DurationIs, stopwatch.Elapsed.TotalSeconds);
			return (report, consolidated);
		}
		#endregion

		#region Nested types
		#region Type: TemplateNames
		private static class TemplateNames
		{
			#region Data
			#region Consts
			private const string Prefix = "templates\\";
			#endregion

			#region Static
			public static readonly string Report = $"{Prefix}ReportTemplate.html";
			#endregion
			#endregion
		}
		#endregion

		#region Type: TemplateVariables
		private static class TemplateVariables
		{
			#region Data
			#region Consts
			public const string GeneratedDateTime = "generatedDate";
			public const string GroupsRepeater = "groups";
			public const string Index = "index";
			public const string IsConsolidation = "isConsolidation";
			public const string Name = "name";
			public const string ReferenceId = "refId";
			public const string ReferenceRepeater = "references";
			public const string ReferenceVersion = "refVersion";
			#endregion
			#endregion
		}
		#endregion
		#endregion
	}
}
