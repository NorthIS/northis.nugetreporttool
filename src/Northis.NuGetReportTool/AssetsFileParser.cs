﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Northis.NuGetReportTool.Data;

namespace Northis.NuGetReportTool
{
	/// <summary>
	/// Представляет парсер файла project.assets.json.
	/// </summary>
	public static class AssetsFileParser
	{
		#region Public
		/// <summary>
		/// Возвращает перечисление зависимостей от NuGet-пакетов на основе контента файла project.assets.json.
		/// </summary>
		public static IEnumerable<Dependency> GetNugetReferences(string jsonContent)
		{
			var jObject = JObject.Parse(jsonContent);
			var libraries = (JObject)jObject.Property("libraries")
											.Value;
			var assets = new List<Dependency>();

			foreach (var lib in libraries)
			{
				var type = (Dependency.ReferenceType)Enum.Parse(typeof(Dependency.ReferenceType),
																lib.Value.OfType<JProperty>()
																   .Single(x => x.Name == "type")
																   .Value.ToString(), true);

				if (type != Dependency.ReferenceType.Package)
				{
					continue;
				}

				var nameStrings = lib.Key.Split('/');

				assets.Add(new Dependency(nameStrings[0], nameStrings[1]));
			}

			return assets.ToList();
		}
		#endregion
	}
}
