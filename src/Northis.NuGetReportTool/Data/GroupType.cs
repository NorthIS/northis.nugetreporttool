﻿namespace Northis.NuGetReportTool.Data
{
	/// <summary>
	/// Предоставляет набор типов групп зависимостей.
	/// </summary>
	internal enum GroupType
	{
		/// <summary>
		/// Группа "зависимости проекта".
		/// </summary>
		Project,
		/// <summary>
		/// Группа "все зависимости".
		/// </summary>
		Total,
		/// <summary>
		/// Группа "зависимости, требующие консолидации".
		/// </summary>
		Consolidate
	}
}
