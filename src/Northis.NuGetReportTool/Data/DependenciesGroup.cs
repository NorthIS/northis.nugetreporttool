﻿using System.Collections.Generic;

namespace Northis.NuGetReportTool.Data
{
	/// <summary>
	/// Представляет группу зависимостей.
	/// </summary>
	internal class DependenciesGroup
	{
		#region Properties
		/// <summary>
		/// Возвращает или устанавливает коллекцию зависимостей.
		/// </summary>
		public IReadOnlyCollection<Dependency> Dependencies
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает или устанавливает имя группы.
		/// </summary>
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает или устанавливает тип группы.
		/// </summary>
		public GroupType Type
		{
			get;
			set;
		}
		#endregion
	}
}
