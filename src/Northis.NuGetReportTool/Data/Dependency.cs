﻿using System;
using PommaLabs.Thrower;

namespace Northis.NuGetReportTool.Data
{
	/// <summary>
	/// Представляет зависимость.
	/// </summary>
	public class Dependency
	{
		#region Enums
		/// <summary>
		/// Описывает тип ссылки на зависимость.
		/// </summary>
		public enum ReferenceType
		{
			/// <summary>
			/// Описывает тип ссылки "Пакет".
			/// </summary>
			Package,
			/// <summary>
			/// Описывает тип ссылки "Проект".
			/// </summary>
			Project
		}
		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="Dependency" />.
		/// </summary>
		/// <exception cref="ArgumentException">
		/// <paramref name="id" /> или <paramref name="version" /> равен
		/// <see langword="null" />, является пустой строкой <see cref="string.Empty" /> или содержит только пробельные символы.
		/// </exception>
		public Dependency(string id, string version)
		{
			Raise.ArgumentException.IfIsNullOrWhiteSpace(id);
			Raise.ArgumentException.IfIsNullOrWhiteSpace(version);
			Id = id;
			Version = version;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает идентификатор зависимости.
		/// </summary>
		public string Id
		{
			get;
		}

		/// <summary>
		/// Возвращает версию зависимости.
		/// </summary>
		public string Version
		{
			get;
		}
		#endregion

		#region Protected
		/// <summary>
		/// Сравнивает текущий объект с <paramref name="other" />.
		/// </summary>
		/// <param name="other">Объект, который требуется сравнить с этим объектом.</param>
		/// <returns>Результат сравнения.</returns>
		protected bool Equals(Dependency other)
		{
			return string.Equals(Id, other.Id) && string.Equals(Version, other.Version);
		}
		#endregion

		#region Overrided
		/// <summary>Determines whether the specified object is equal to the current object.</summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>
		/// <see langword="true" /> if the specified object  is equal to the current object; otherwise, <see langword="false" />.
		/// </returns>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;

			return Equals((Dependency)obj);
		}

		/// <summary>Serves as the default hash function. </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				return ((Id != null ? Id.GetHashCode() : 0) * 397) ^ (Version != null ? Version.GetHashCode() : 0);
			}
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return $"{Id}/{Version}";
		}
		#endregion
	}
}
