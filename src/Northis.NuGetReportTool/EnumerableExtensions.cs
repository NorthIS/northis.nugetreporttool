﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using PommaLabs.Thrower;

namespace Northis.NuGetReportTool
{
	/// <summary>
	/// Представляет методы расширения для <see cref="IEnumerable{T}" />.
	/// </summary>
	public static class EnumerableExtensions
	{
		#region Public
		/// <summary>
		/// Преобразовывает исходное перечисление в плоское перечисление.
		/// </summary>
		/// <param name="source">Исходное перечисление.</param>
		/// <param name="selector">Метод, указывающий на вложенное в объект перечисление.</param>
		/// <exception cref="ArgumentNullException">
		/// Как минимум один из переданных параметров (смотри <see cref="Exception.Message" />) равен <see langword="null" />.
		/// </exception>
		public static IEnumerable<T> Flatten<T>([NotNull] this IEnumerable<T> source, [NotNull] Func<T, IEnumerable<T>> selector)
		{
			Raise.ArgumentNullException.IfIsNull(source, nameof(source));
			Raise.ArgumentNullException.IfIsNull(selector, nameof(selector));
			return source.SelectMany(c => selector(c)
										 .Flatten(selector))
						 .Concat(source);
		}
		#endregion
	}
}
