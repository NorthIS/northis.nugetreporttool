﻿using System.Configuration;

namespace Northis.NuGetReportTool
{
	/// <summary>
	/// Предоставляет доступ к конфигурации приложения.
	/// </summary>
	internal static class AppSettings
	{
		#region Data
		#region Static
		/// <summary>
		/// Возвращает левую границу закрывающего элемента повторителя.
		/// </summary>
		public static string CloseRepeaterLeftBoundary = ConfigurationManager.AppSettings["closeRepeaterLeftBoundary"];
		/// <summary>
		/// Возвращает правую границу закрывающего элемента повторителя.
		/// </summary>
		public static string CloseRepeaterRightBoundary = ConfigurationManager.AppSettings["closeRepeaterRightBoundary"];
		/// <summary>
		/// Возвращает левую границу открывающего элемента повторителя.
		/// </summary>
		public static string OpenRepeaterLeftBoundary = ConfigurationManager.AppSettings["openRepeaterLeftBoundary"];
		/// <summary>
		/// Возвращает правую границу открывающего элемента повторителя.
		/// </summary>
		public static string OpenRepeaterRightBoundary = ConfigurationManager.AppSettings["openRepeaterRightBoundary"];
		/// <summary>
		/// Возвращает границу заполнителя.
		/// </summary>
		public static string PlaceholderBoundary = ConfigurationManager.AppSettings["placeholderBoundary"];
		#endregion
		#endregion
	}
}
