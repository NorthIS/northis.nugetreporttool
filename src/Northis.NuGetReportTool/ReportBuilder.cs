﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using Northis.NuGetReportTool.Properties;
using PommaLabs.Thrower;

namespace Northis.NuGetReportTool
{
	/// <summary>
	/// Представляет класс для постоения отчета.
	/// </summary>
	internal class ReportBuilder
	{
		#region Data
		#region Consts
		private const string PlaceholderGroup = "placeholder";
		private const string RepeaterBodyGroup = "repeaterBody";
		private const string RepeaterGroup = "repeater";
		#endregion

		#region Fields
		private readonly Regex _parserRegex;
		private readonly string _wrongPlaceholderFormat;
		private readonly string _wrongRepeaterFormat;
		private readonly Logger _logger = LogManager.GetCurrentClassLogger();
		#endregion
		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="ReportBuilder" />.
		/// </summary>
		/// <param name="placeholderBoundary">Граница заполнителя.</param>
		/// <param name="openRepeaterLeftBoundary">Левая граница открывающего элемента повторителя.</param>
		/// <param name="openRepeaterRightBoundary">Правая граница открывающего элемента повторителя.</param>
		/// <param name="closeRepeaterLeftBoundary">Левая граница закрывающего элемента повторителя.</param>
		/// <param name="closeRepeaterRightBoundary">Правая граница закрывающего элемента повторителя.</param>
		/// <param name="wrongPlaceholderFormat">
		/// Строка формата, на которую будут заменены все невалидные заполнители или
		/// <c>null</c> для замены на пустую строку.
		/// </param>
		/// <param name="wrongRepeaterFormat">
		/// Строка формата, на которую будут заменены все невалидные повторители или <c>null</c>
		/// для замены на пустую строку.
		/// </param>
		/// <exception cref="ArgumentNullException">
		/// Возникает, если хотя бы один из следующих параметров равен <c>null</c>, пустой строке или состоит из
		/// пробельных символов: <paramref name="placeholderBoundary" />, <paramref name="openRepeaterLeftBoundary" />,
		/// <paramref name="openRepeaterRightBoundary" />, <paramref name="closeRepeaterLeftBoundary" />,
		/// <paramref name="closeRepeaterRightBoundary" />.
		/// </exception>
		// ToDo подумать, куда вынести портянку параметров
		public ReportBuilder(string placeholderBoundary,
			string openRepeaterLeftBoundary,
			string openRepeaterRightBoundary,
			string closeRepeaterLeftBoundary,
			string closeRepeaterRightBoundary,
			string wrongPlaceholderFormat = null,
			string wrongRepeaterFormat = null)
		{
			Raise.ArgumentException.IfIsNullOrWhiteSpace(placeholderBoundary, nameof(placeholderBoundary));
			Raise.ArgumentException.IfIsNullOrWhiteSpace(openRepeaterLeftBoundary, nameof(openRepeaterLeftBoundary));
			Raise.ArgumentException.IfIsNullOrWhiteSpace(openRepeaterRightBoundary, nameof(openRepeaterRightBoundary));
			Raise.ArgumentException.IfIsNullOrWhiteSpace(closeRepeaterLeftBoundary, nameof(closeRepeaterLeftBoundary));
			Raise.ArgumentException.IfIsNullOrWhiteSpace(closeRepeaterRightBoundary, nameof(closeRepeaterRightBoundary));

			_wrongPlaceholderFormat = string.IsNullOrWhiteSpace(wrongPlaceholderFormat) ? string.Empty : wrongPlaceholderFormat;
			_wrongRepeaterFormat = string.IsNullOrWhiteSpace(wrongRepeaterFormat) ? string.Empty : wrongRepeaterFormat;

			_parserRegex = new Regex($"{Regex.Escape(placeholderBoundary)}(?<{PlaceholderGroup}>.*?){Regex.Escape(placeholderBoundary)}" +
									 "|" +
									 $"{Regex.Escape(openRepeaterLeftBoundary)}(?<{RepeaterGroup}>.*?){Regex.Escape(openRepeaterRightBoundary)}" +
									 $"(?<{RepeaterBodyGroup}>.*?)" +
									 $"{Regex.Escape(closeRepeaterLeftBoundary)}\\k<{RepeaterGroup}>{Regex.Escape(closeRepeaterRightBoundary)}",
									 RegexOptions.Singleline);
		}
		#endregion

		#region Public
		/// <summary>
		/// Выполняет построение отчета.
		/// </summary>
		/// <param name="template">Шаблон отчета.</param>
		/// <param name="fillPlaceholderCallback">
		/// Функция замены заполнителя; первым аргументом передается имя заполнителя, вторым
		/// текущий контекстный объект, результат - строка, на котрую будет заменен заполнитель.
		/// </param>
		/// <param name="expandRepeaterCallback">
		/// Функция расширения повторителя; первым аргументом передается имя повторителя,
		/// вторым текущий контекстный объект, результат - перечисление объектов, каждый из которых будет использоваться как
		/// контекстный объект для построения тела повторителя.
		/// </param>
		/// <exception cref="ArgumentNullException">Возникает, если хотя бы один из аргументов равен <c>null</c>.</exception>
		/// <returns>Построенный отчет.</returns>
		public string Build(string template, Func<string, object, string> fillPlaceholderCallback, Func<string, object, IEnumerable> expandRepeaterCallback)
		{
			Raise.ArgumentNullException.IfIsNull(template, nameof(template));
			Raise.ArgumentNullException.IfIsNull(fillPlaceholderCallback, nameof(fillPlaceholderCallback));
			Raise.ArgumentNullException.IfIsNull(expandRepeaterCallback, nameof(expandRepeaterCallback));

			return _parserRegex.Replace(template, match => EvaluateMatch(match, null, fillPlaceholderCallback, expandRepeaterCallback));
		}
		#endregion

		#region Private
		private string EvaluateMatch(Match match,
			object currentObject,
			Func<string, object, string> fillPlaceholderCallback,
			Func<string, object, IEnumerable> expandRepeaterCallback)
		{
			var groups = match.Groups;
			if (groups[PlaceholderGroup]
				.Success)
			{
				var placeholderName = groups[PlaceholderGroup]
					.Value;
				var result = fillPlaceholderCallback(placeholderName, currentObject);
				if (result == null)
				{
					_logger.Error(Resources.StrError_WrongPlaceholderName, placeholderName);
					result = string.Format(_wrongPlaceholderFormat, placeholderName);
				}

				return result;
			}

			if (groups[RepeaterGroup]
				.Success)
			{
				var repeaterName = groups[RepeaterGroup]
					.Value;

				var itemsForRepeater = expandRepeaterCallback(repeaterName, currentObject);
				if (itemsForRepeater == null)
				{
					_logger.Error(Resources.StrError_WrongRepeaterName, repeaterName);
					return string.Format(_wrongRepeaterFormat, repeaterName);
				}

				var repeaterBodyTemplate = groups[RepeaterBodyGroup]
					.Value;
				var filledBodies =
					itemsForRepeater.Cast<object>()
									.Select(item => _parserRegex.Replace(repeaterBodyTemplate, m => EvaluateMatch(m, item, fillPlaceholderCallback, expandRepeaterCallback)));
				var expandedRepeater = string.Join(string.Empty, filledBodies);
				return expandedRepeater;
			}

			return string.Empty;
		}
		#endregion
	}
}
